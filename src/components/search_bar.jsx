import React, {Component} from 'react'

class SearchBar extends Component {
  constructor(props) {
    super(props);
    this.state = { val: ''};
  }

  render() {
    return (
      <div className="search-bar">
        <input
          value={this.state.val}
          onChange={event => this.onInputChange(event.target.value)} />
      </div>
    );
  };

  onInputChange(term) {
    this.setState({val: term});
    this.props.onSearchTermChange(term);
  }
}

export default SearchBar;
